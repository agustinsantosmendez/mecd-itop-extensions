<?php
/**
 * Localized data
 *
 * @copyright   Copyright (C) 2013 XXXXX
 * @license     http://opensource.org/licenses/AGPL-3.0
 */

Dict::Add('EN US', 'English', 'English', array(
	// Dictionary entries go here
	// FunctionalCI
        'Class:FunctionalCI/Attribute:environ' => 'Environment',
        'Class:FunctionalCI/Attribute:environ+' => 'Environment where a software component is deployed and executed',
        'Class:FunctionalCI/Attribute:environ/Value:development' => 'development',
        'Class:FunctionalCI/Attribute:environ/Value:test' => 'Test',
        'Class:FunctionalCI/Attribute:environ/Value:acceptance' => 'Acceptance',
        'Class:FunctionalCI/Attribute:environ/Value:production' => 'Production',
        'Class:FunctionalCI/Attribute:environ/Value:education' => 'Education',
        'Class:FunctionalCI/Attribute:environ/Value:backup' => 'Backup',
        'Class:FunctionalCI/Attribute:environ/Value:other' => 'Other',
	// MobilePhone
	'Class:MobilePhone/Attribute:num_puk' => 'PUK',
	// Person
	'Class:Person/Attribute:employee_level' => 'Employee Level',
	'Class:Person/Attribute:orgunit' => 'Unit',
	'Class:Person/Attribute:office' => 'Office Nr.',
	'Class:Person/Attribute:floor' => 'Floor',
	'Class:Person/Attribute:activedirectoryuser' => 'Active Directory User',
	// PhysicalDevice
        'Class:PhysicalDevice/Attribute:owner' => 'Owner',
        // Server
        'Class:Server/Attribute:architecture' => 'Architecture',
        // VirtualMachine
        'Class:VirtualMachine/Attribute:architecture' => 'Architecture',
        // PC
        'Class:PC/Attribute:disk' => 'Disk',
        'Class:PC/Attribute:architecture' => 'Architecture',
        // Monitor
        'Class:Monitor' => 'Monitor',
	'Class:Monitor+' => 'A computer display',
        'Class:Monitor/Attribute:technology' => 'Technology',
        'Class:Monitor/Attribute:technology+' => 'Technology used for the display',
        'Class:Monitor/Attribute:technology/Value:crt' => 'CRT',
        'Class:Monitor/Attribute:technology/Value:lcd' => 'LCD',
        'Class:Monitor/Attribute:technology/Value:led' => 'LED',
        'Class:Monitor/Attribute:technology/Value:other' => 'Other',
        // Television
        'Class:Television' => 'Television',
	'Class:Television+' => 'A Television',
        'Class:Television/Attribute:technology' => 'Technology',
        'Class:Television/Attribute:technology+' => 'Technology used for Television',
        'Class:Television/Attribute:technology/Value:crt' => 'CRT',
        'Class:Television/Attribute:technology/Value:lcd' => 'LCD',
        'Class:Television/Attribute:technology/Value:led' => 'LED',
        'Class:Television/Attribute:technology/Value:other' => 'Other',
        // Internet Devices
        'Class:InetDevice' => 'Internet Device',
	'Class:InetDevice+' => 'An enduser device with internet conexion',
	'Class:InetDevice/Attribute:internalphonenumber' => 'Internal Phone Number',
	'Class:InetDevice/Attribute:num_icc' => 'ICC Number',
        // Application Solution
        'Class:ApplicationSolution/Attribute:owner' => 'Owner',
        'Class:ApplicationSolution/Attribute:appstatus' => 'App. State',
        'Class:ApplicationSolution/Attribute:appstatus+' => 'Application State (active, soft or hard deleted, etc.)',
        'Class:ApplicationSolution/Attribute:appstatus/Value:pending' => 'Pending (testing)',
        'Class:ApplicationSolution/Attribute:appstatus/Value:active' => 'Active',
        'Class:ApplicationSolution/Attribute:appstatus/Value:softdeleted' => 'Logical Deletion',
        'Class:ApplicationSolution/Attribute:appstatus/Value:harddeleted' => 'Physical Deletion',
        'Class:ApplicationSolution/Attribute:personaldataprotection' => 'RGPD/LOPD',
        'Class:ApplicationSolution/Attribute:personaldataprotection+' => 'Personal Data Protection',
        'Class:ApplicationSolution/Attribute:personaldataprotection/Value:yes' => 'Yes',
        'Class:ApplicationSolution/Attribute:personaldataprotection/Value:no' => 'No',
        'Class:ApplicationSolution/Attribute:personaldataprotection/Value:pending' => 'Pending',
));
?>

