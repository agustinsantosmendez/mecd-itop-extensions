# mecd-itop-extensions

Estos son los fichero para extender iTop según las necesidades que se han detectado en MECD.
Es un trabajo en curso y su uso está limitado al MECD.

## Uso

En el directorio de extensiones de iTop ($ITOP_DIR/extensions). 
```
git clone https://gitlab.com/agustinsantosmendez/mecd-itop-extensions.git mecd-module
```

Nota: Cuando utilizo iTop con Docker, el directorio extensions lo suelo montar en  ~/itop/app.

Desda la página de toolkit de iTop (http://itop_hostname_or_ip/toolkit) se puede generar los cambios de la BD y de código PHP.
Se recomienda seguir las instrucciones de la página:

https://www.itophub.io/wiki/page?id=2_4_0%3Acustomization%3Aadd-attribute-sample