<?php
/**
 * Localized data
 *
 * @copyright   Copyright (C) 2013 XXXXX
 * @license     http://opensource.org/licenses/AGPL-3.0
 */

Dict::Add('ES CR', 'Spanish', 'Español, Castellano', array(
	// Dictionary entries go here
	// FunctionalCI
        'Class:FunctionalCI/Attribute:environ' => 'Entorno',
        'Class:FunctionalCI/Attribute:environ+' => 'Entorno de trabajo',
        'Class:FunctionalCI/Attribute:environ/Value:development' => 'Desarrollo',
        'Class:FunctionalCI/Attribute:environ/Value:test' => 'Pruebas',
        'Class:FunctionalCI/Attribute:environ/Value:acceptance' => 'Aceptación',
        'Class:FunctionalCI/Attribute:environ/Value:production' => 'Produción',
        'Class:FunctionalCI/Attribute:environ/Value:education' => 'Educación',
        'Class:FunctionalCI/Attribute:environ/Value:backup' => 'Backup',
        'Class:FunctionalCI/Attribute:environ/Value:other' => 'Other',
	// MobilePhone
	'Class:MobilePhone/Attribute:num_puk' => 'PUK',
	// Person
	'Class:Person/Attribute:employee_level' => 'Nivel',
	'Class:Person/Attribute:orgunit' => 'Unidad o Servicio',
	'Class:Person/Attribute:office' => 'Despacho',
	'Class:Person/Attribute:floor' => 'Planta',
	'Class:Person/Attribute:activedirectoryuser' => 'Usuario DA',
	// PhysicalDevice
	'Class:PhysicalDevice/Attribute:owner' => 'Propietario',
        // Server
        'Class:Server/Attribute:architecture' => 'Arquitectura',
        // VirtualMachine
        'Class:VirtualMachine/Attribute:architecture' => 'Arquitectura',
	// PC
	'Class:PC/Attribute:disk' => 'Disco',
        'Class:PC/Attribute:architecture' => 'Arquitectura',
	// Monitor
	'Class:Monitor' => 'Monitor',
        'Class:Monitor+' => 'Pantalla de ordenador',
        'Class:Monitor/Attribute:technology' => 'Tecnología',
        'Class:Monitor/Attribute:technology+' => 'Tecnología de la pantalla',
        'Class:Monitor/Attribute:technology/Value:crt' => 'CRT',
        'Class:Monitor/Attribute:technology/Value:lcd' => 'LCD',
        'Class:Monitor/Attribute:technology/Value:led' => 'LED',
        'Class:Monitor/Attribute:technology/Value:other' => 'Otra',
        // Television
	'Class:Television' => 'Televisión',
        'Class:Television+' => 'Televisión',
        'Class:Television/Attribute:technology' => 'Tecnología',
        'Class:Television/Attribute:technology+' => 'Tecnología de la Televisión',
        'Class:Television/Attribute:technology/Value:crt' => 'CRT',
        'Class:Television/Attribute:technology/Value:lcd' => 'LCD',
        'Class:Television/Attribute:technology/Value:led' => 'LED',
        'Class:Television/Attribute:technology/Value:other' => 'Otra',
        // Internet Devices
        'Class:InetDevice' => 'Dispositivo de Internet',
	'Class:InetDevice+' => 'Dispositivo conectable a Internet',
	'Class:InetDevice/Attribute:internalphonenumber' => 'Número Interno',
	'Class:InetDevice/Attribute:num_icc' => 'Número de Tarjeta ICC',
        // Application Solution
        'Class:ApplicationSolution/Attribute:owner' => 'Propietarios',
        'Class:ApplicationSolution/Attribute:appstatus' => 'Situación',
        'Class:ApplicationSolution/Attribute:appstatus+' => 'Situación de  la apliación (activa, borrada, etc.)',
        'Class:ApplicationSolution/Attribute:appstatus/Value:pending' => 'Pendiente de pruebas',
        'Class:ApplicationSolution/Attribute:appstatus/Value:active' => 'Alta Lógica',
        'Class:ApplicationSolution/Attribute:appstatus/Value:softdeleted' => 'Baja Lógica',
        'Class:ApplicationSolution/Attribute:appstatus/Value:harddeleted' => 'Baja Física',
        'Class:ApplicationSolution/Attribute:personaldataprotection' => 'RGPD/LOPD',
        'Class:ApplicationSolution/Attribute:personaldataprotection+' => 'Normativa sobre protección de datos personales',
        'Class:ApplicationSolution/Attribute:personaldataprotection/Value:yes' => 'Si',
        'Class:ApplicationSolution/Attribute:personaldataprotection/Value:no' => 'No',
        'Class:ApplicationSolution/Attribute:personaldataprotection/Value:pending' => 'En tramitación',
));
?>

